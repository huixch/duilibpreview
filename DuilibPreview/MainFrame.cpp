#include "stdafx.h"
#include "MainFrame.h"


CMainFrame::CMainFrame()
{
	m_SkinFile = _T("");
}


CMainFrame::~CMainFrame()
{
}

CDuiString CMainFrame::GetSkinFile()
{
	return m_SkinFile.IsEmpty() ? _T("") : m_SkinFile;
}

void CMainFrame::SetSkinFile(CDuiString filename)
{
	m_SkinFile = filename;
}

LRESULT CMainFrame::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == WM_KEYDOWN && wParam == VK_ESCAPE)
	{
		PostQuitMessage(0);
	}
	return WindowImplBase::HandleMessage(uMsg, wParam, lParam);
}
