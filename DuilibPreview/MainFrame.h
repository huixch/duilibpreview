#pragma once
#include "UIlib.h"

using namespace DuiLib;

class CMainFrame :
	public WindowImplBase
{
private:
	CDuiString m_SkinFile;
protected:
	virtual CDuiString GetSkinFile();
	virtual LPCTSTR GetWindowClassName(void) const { return _T("CMainFrame"); };
public:
	CMainFrame();
	~CMainFrame();
	void SetSkinFile(CDuiString filename);
	virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);
};

