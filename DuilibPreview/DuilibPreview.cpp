// DuilibPreview.cpp : 定义应用程序的入口点。
//

#include "stdafx.h"
#include "DuilibPreview.h"
#include "MainFrame.h"

using namespace DuiLib;


int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	int argc = 0;
	LPTSTR *argv = NULL;
	TCHAR path[MAX_PATH] = { 0 };
	LPTSTR szCommandLine = GetCommandLine();
	argv = CommandLineToArgvW(szCommandLine, &argc);
	if (argc < 2)
	{
		MessageBox(NULL, _T("参数错误，请指定要预览的XML文件名！"), _T("DuiLib XML 预览工具"), MB_OK | MB_ICONERROR);
		return 0;
	}
	lstrcpyn(path, argv[1], MAX_PATH);
	::PathRemoveFileSpec(path);	
	CDuiString filename = ::PathFindFileName(argv[1]);
	CPaintManagerUI::SetInstance(hInstance);
	CPaintManagerUI::SetResourcePath(path);
	CMainFrame *mainframe = new CMainFrame();
	mainframe->SetSkinFile(filename);
	mainframe->Create(NULL, _T("预览窗口"), UI_WNDSTYLE_FRAME, UI_WNDSTYLE_EX_FRAME, 0, 0, 800, 600);
	mainframe->CenterWindow();
	mainframe->SetIcon(IDI_DUILIBPREVIEW);
	mainframe->ShowWindow(true);
	CPaintManagerUI::MessageLoop();
	LocalFree(argv);
	return 0;
}


