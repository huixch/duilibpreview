# duilibpreview

#### 介绍
DuiLib XML 预览工具

#### 使用说明

使用时将界面文件(*.xml)拖到DuilibPreview.exe上即可预览，按"Esc"或者"Alt+F4"退出。

#### 集成到编辑器
##### EditPlus
1. 工具->配置用户工具->添加
2. 菜单文本=DuilibPreview
3. 命令=DuilibPreview.exe的完整路径
4. 参数=$(FilePath)

##### Notepad++
1. 打开notpad++,按F5运行
2. 程序名=DuilibPreview.exe的完整路径 $(FULL_CURRENT_PATH)
3. 保存

##### Sublime Text 3
参考教程：[https://blog.csdn.net/u012081284/article/details/104374826](https://blog.csdn.net/u012081284/article/details/104374826)


